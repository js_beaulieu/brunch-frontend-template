/*eslint-env node*/
module.exports = {
    paths: {
        public: process.env.PUBLIC_PATH || './public',
        watched: ['src']
    },

    files: {
        javascripts: {
            joinTo: {
                'vendor.js': 'src/js/vendor/*.js',
                'build.js': 'src/js/custom/*.js'
            }
        },
        stylesheets: {
            joinTo: { 'build.css': 'src/scss/index.scss' }
        }
    },

    plugins: {
        babel: {
            presets: ['env']
        },
        postcss: {
            processors: [require('autoprefixer')]
        },
        autoReload: {
            enabled: {
                css: true,
                js: true,
                assets: false
            }
        }
    },

    overrides: {
        production: {
            plugins: { autoReload: { enabled: false } },
            files: {
                javascripts: {
                    joinTo: {
                        'vendor.min.js': 'src/js/vendor/*.js',
                        'build.min.js': 'src/js/custom/*.js'
                    }
                },
                stylesheets: {
                    joinTo: { 'build.min.css': 'src/scss/index.scss' }
                }
            },
        }
    }
};
